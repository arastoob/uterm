
var cmdHistory = new Set();
var historyIteratorIdx = -1;

$( document ).ready(function() {
    $("#command-txt").focus();
});
  
  $("#clear").click(function() {
    $("#res-txt").empty();
  });
  
$("#command-txt").keydown(function(event) {
  var keycode = (event.keyCode ? event.keyCode : event.which);
  if(keycode == '13'){
    let input = $("#command-txt").val();
    if (input != "") {
      execute_command(input);
    }
  }
  if(keycode == '38'){
    if (historyIteratorIdx < cmdHistory.size - 1) {
      historyIteratorIdx += 1;
    } else if (historyIteratorIdx == cmdHistory.size - 1) {
      historyIteratorIdx = 0;
    }
    $("#command-txt").val(Array.from(cmdHistory)[historyIteratorIdx]);
  }
  if(keycode == '40'){
    if (historyIteratorIdx > 0) {
      historyIteratorIdx -= 1;
    } else if (historyIteratorIdx == 0) {
      historyIteratorIdx = cmdHistory.size - 1;
    }
    $("#command-txt").val(Array.from(cmdHistory)[historyIteratorIdx]);
  }
});


  function execute_command(input) {
    cmdHistory.add(input);
      historyIteratorIdx += 1;
    let strs = input.split(" ");
    var obj = {
      cmd: strs[0],
      args: strs.slice(1)
    };
    var json = JSON.stringify(obj);

    var ws = new WebSocket('ws://127.0.0.1:3000/ui/ws');
    ws.onopen = function() {
        
      $("#res-txt").append(" > " +input + "\n");
        // Web Socket is connected, send data using send()
        ws.send(input);
     };
    ws.onmessage = function (evt) { 
        var received_msg = evt.data;
      $("#res-txt").append(received_msg + "\n");
      $("#res-txt").animate({
        scrollTop: $("#res-txt").prop("scrollHeight")
        }, 50);
     };

     ws.onclose = function() { 
        // websocket is closed.
       $("#res-txt").append("<hr />");
       $("#command-txt").val("");
     };
  }
