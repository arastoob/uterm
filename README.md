# uterm: a minimal terminal in the browser, implemented in [Rust](https://www.rust-lang.org/)

Using uterm, we can run terminal commands in the brwoser by excuting the commands with `std::process::command`.

The core is implemented in Rust and the UI is implemented in html and javascript.

Before running uterm, make sure you have installed Rust (https://www.rust-lang.org/tools/install)

## Running
- Clone the repo and go inside uterm root
- Run `cargo run -r` in a terminal window: this will run a `uterm` server on localhost on port 3000 (`127.0.0.1:3000`), waiting for Websocket connections 
- Open a browser window and go to `127.0.0.1:3000`
