use axum::http::Response;
use axum::{
    extract::ws::{Message, WebSocket, WebSocketUpgrade},
    response::Html,
    response::IntoResponse,
    routing::get,
    Router,
};
use std::io::BufRead;
use std::io::BufReader;
use std::net::SocketAddr;
use std::process::{Command, Stdio};
use uterm::app_error::AppError;

#[tokio::main]
async fn main() -> Result<(), AppError> {
    // initialize tracing
    tracing_subscriber::fmt::init();

    // build our application with a route
    let app = Router::new()
        .route("/", get(index))
        .route("/ui/scripts.js", get(get_js))
        .route("/ui/styles.css", get(get_styles))
        .route("/ui/ws", get(ws_handler));

    let addr = SocketAddr::from(([127, 0, 0, 1], 3000));
    tracing::debug!("listening on {}", addr);
    axum::Server::try_bind(&addr)?
        .serve(app.into_make_service())
        .await?;

    Ok(())
}

// basic handler that responds with a static string
async fn index() -> Result<Html<String>, AppError> {
    let html = tokio::fs::read("./ui/index.html")
        .await
        .map_err(|err| AppError::RuntimeError(err.to_string()))?;
    let html = String::from_utf8(html).map_err(|err| AppError::RuntimeError(err.to_string()))?;
    Ok(Html(html))
}

async fn get_js() -> Result<Response<String>, AppError> {
    let js = tokio::fs::read_to_string("./ui/scripts.js")
        .await
        .map_err(|err| AppError::RuntimeError(err.to_string()))?;
    Response::builder()
        .header("content-type", "application/javascript")
        .body(js)
        .map_err(|err| AppError::RuntimeError(err.to_string()))
}

async fn get_styles() -> Result<Response<String>, AppError> {
    let css = tokio::fs::read_to_string("./ui/styles.css")
        .await
        .map_err(|err| AppError::RuntimeError(err.to_string()))?;

    Response::builder()
        .header("content-type", "text/css;harset=utf8;")
        .body(css)
        .map_err(|err| AppError::RuntimeError(err.to_string()))
}

async fn ws_handler(ws: WebSocketUpgrade) -> impl IntoResponse {
    ws.on_upgrade(move |socket| handle_socket(socket))
}

/// Actual websocket statemachine (one will be spawned per connection)
async fn handle_socket(mut socket: WebSocket) {
    // TODO: handle errors properly
    if let Some(Ok(Message::Text(cmd))) = socket.recv().await {
        let mut command = Command::new("sh");
        command.args(["-c", &cmd]);
        let mut child = command
            .stdout(Stdio::piped())
            .stderr(Stdio::piped())
            .spawn()
            .expect("failed to spawn child process");

        let stderr = child.stderr.take().unwrap();
        let stderr_reader = BufReader::new(stderr);
        for line in stderr_reader.lines() {
            socket.send(Message::Text(line.unwrap())).await.unwrap();
        }

        let stdout = child.stdout.take().unwrap();
        let stdout_reader = BufReader::new(stdout);
        for line in stdout_reader.lines() {
            socket.send(Message::Text(line.unwrap())).await.unwrap();
        }

        if child.wait().is_ok() {
            // close the socket connection
            socket.close().await.unwrap();
        }
    }
}
