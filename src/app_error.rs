use axum::http::StatusCode;
use axum::response::{IntoResponse, Response};

#[derive(Debug)]
pub enum AppError {
    AxumError(axum::Error),

    HyperError(hyper::Error),

    RuntimeError(String),
}

impl From<axum::Error> for AppError {
    fn from(err: axum::Error) -> Self {
        Self::AxumError(err)
    }
}

impl From<hyper::Error> for AppError {
    fn from(err: hyper::Error) -> Self {
        Self::HyperError(err)
    }
}

impl IntoResponse for AppError {
    fn into_response(self) -> Response {
        let body = match self {
            AppError::AxumError(ref detail) => format!("axum error: {}", detail.to_string()),
            AppError::HyperError(ref detail) => format!("hyper error: {}", detail.to_string()),
            AppError::RuntimeError(ref detail) => {
                format!("runtime error: {}", detail)
            }
        };
        (StatusCode::INTERNAL_SERVER_ERROR, body).into_response()
    }
}
